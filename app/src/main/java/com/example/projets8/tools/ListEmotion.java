package com.example.projets8.tools;

import java.util.ArrayList;

public class ListEmotion {

    public static String getEmotion(){
        String result = null;

        ArrayList<String> emotion = new ArrayList<String>();
        emotion.add("Joie");
        emotion.add("Peur");
        emotion.add("Colère");
        emotion.add("Surprise");
        emotion.add("Tristesse");
        emotion.add("Dégoût");
        emotion.add("Neutre");

        result = emotion.get((int) (Math.random() * ( 7 - 1 )));

        return result;
    }

    public static String getAventureEmotion(int chapitre){
        String result = null;

        ArrayList<String> emotion = new ArrayList<String>();
        emotion.add("neutral");
        emotion.add("sad");
        emotion.add("angry");
        emotion.add("disgust");
        emotion.add("surprise");
        emotion.add("neutral");
        emotion.add("sad");
        emotion.add("neutral");
        emotion.add("neutral");
        emotion.add("fear");
        emotion.add("happy");
        emotion.add("neutral");
        emotion.add("neutral");
        emotion.add("surprise");
        emotion.add("disgust");
        emotion.add("fear");
        emotion.add("surprise");
        emotion.add("surprise");
        emotion.add("happy");

        result = emotion.get(chapitre - 1); //-1 pour etre sur le bon numero de chapitre

        return result;
    }




}
