package com.example.projets8;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.projets8.tools.ListEmotion;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.Hashtable;
import java.util.Map;

public class RetourCameraAléatoire extends AppCompatActivity {

    //TODO Si necessaire et si quelqun chaud : mettre le image view en rond

    String TAG = "RetourCamera";

    String currentMode = null;
    boolean match = true; //Variable pour savoir si l'image envoyé match avec celle demandé
    boolean suivant = false; // 0 = prendre une photo , 1 = suivant

    int points = 0;

    TextView emotion;
    Button takenPhotoButton;
    FrameLayout background;
    TextView point;
    ImageView image;

    String currentEmotion;


    private static final int CAMERA_REQUEST = 10001;
    int CAMERA_PIC_REQUEST = 2;

    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_retour_camera_aleatoire);

        Log.d(TAG, String.valueOf(points));

        //Récupere le mode de jeu
        if (getIntent().getExtras().getString("mode", null).equals("aleatoire")) {
            currentMode = "aleatoire";
        } else if (getIntent().getExtras().getString("mode", null).equals("aventure")) {
            currentMode = "aventure";
        }

        emotion = findViewById(R.id.emotion_aleatoire);

        takenPhotoButton = (Button) findViewById(R.id.takenPhotoButton_aleatoire);
        background = findViewById(R.id.background);
        point = findViewById(R.id.point);

        if (currentMode.equals("aleatoire")) {

            currentEmotion = createEmotion();
            point.setText(points + " points");
        }

        takenPhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!suivant) {
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    // request code

                    startActivityForResult(cameraIntent, 1337);
                } else {
                    suivant = !suivant;
                    takenPhotoButton.setText("Prendre la photo");
                    putWhiteBackground();
                    image.setImageBitmap(null);
                    currentEmotion = createEmotion();

                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1337) {
            if (data.getExtras() != null) {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                Log.d(TAG, "taken");
                image = (ImageView) findViewById(R.id.takenPhoto);
                image.setImageBitmap(thumbnail);

                String imageToSend = getStringImage(thumbnail);
                //Log.d("image", imageToSend);
                //passing the image to volley
                SendImage(imageToSend);
            }

        } else {
            Toast.makeText(this, "Picture NOt taken", Toast.LENGTH_LONG);
            Log.d(TAG, "not taken");
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    public String createEmotion() {
        String currentEmotion = ListEmotion.getEmotion();
        emotion.setText(currentEmotion);
        return currentEmotion;
    }

    private void goToLooseScreen() {
        Intent intent = new Intent(this, LooseScreen.class);
        intent.putExtra("score", points);
        startActivity(intent);
    }


    //===============   Fonctions pour le POST ===========
    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;

    }

    private void SendImage(final String image) {

        final StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://178.128.253.84",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("uploade", response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);



                            String emotionBack = jsonObject.getString("dominant_emotion");

                            Log.d(TAG, "=====================================");
                            Log.d(TAG, "Emotion perçue : " + emotionBack);
                            Log.d(TAG, "Emotion demandé : "+currentEmotion);

                            if (emotionBack.equals(frenchToEnglish(currentEmotion))) {
                                match = true;
                                points++;
                                Log.d(TAG, String.valueOf(points));
                                point.setText(points + " points");
                                putGreenBackground();
                                takenPhotoButton.setText("Suivant ->");
                                suivant = !suivant;
                            } else {
                                match = false;
                                Log.d(TAG, "Mauvaise émotion");
                                goToLooseScreen();
                            }


                        } catch (JSONException ex) {
                            ex.printStackTrace();
                            Toast.makeText(context, "Visage non détecter", Toast.LENGTH_LONG).show();
                            Log.d("TAG","RECU");

                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(Edit_Profile.this, "No internet connection", Toast.LENGTH_LONG).show();
                        Log.d(TAG, error.getMessage());
                        Log.d(TAG, error.getCause().toString());
                        Log.d(TAG, "Error connection");

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new Hashtable<String, String>();

                params.put("image", image);
                return params;
            }
        };
        {
            int socketTimeout = 30000;
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            stringRequest.setRetryPolicy(policy);
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }

    }

    //============================================================


    public void putGreenBackground() {
        background.setBackgroundColor(Color.argb(255, 172, 250, 178));
    }

    public void putWhiteBackground() {
        background.setBackgroundColor(Color.argb(255, 255, 255, 255));
    }


    public String frenchToEnglish(String french) {
        String english = null;
        switch (french) {
            case "Joie":
                english = "happy";
                break;
            case "Colère":
                english = "angry";
                break;
            case "Dégoût":
                english = "disgust";
                break;
            case "Peur":
                english = "fear";
                break;
            case "Neutre":
                english = "neutral";
                break;
            case "Tristesse":
                english = "sad";
                break;
            case "Surprise":
                english = "surprise";
                break;
        }
        return english;
    }


}






