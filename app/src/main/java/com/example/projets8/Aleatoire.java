package com.example.projets8;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Aleatoire extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aleatoire);

        Button boutonJouer = findViewById(R.id.bouton_jouer);

        boutonJouer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToAleatoire();
            }
        });

    }

    private void goToAleatoire() {
        Intent intent = new Intent(this, RetourCameraAléatoire.class);
        intent.putExtra("mode", "aleatoire");
        startActivity(intent);
    }
}
