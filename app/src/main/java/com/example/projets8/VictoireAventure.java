package com.example.projets8;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.projets8.tools.Tools;

public class VictoireAventure extends AppCompatActivity {

    Button retourMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_victoire_aventure);

        retourMenu = findViewById(R.id.retourMenu);

        retourMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToActivity(Modes.class);
            }
        });
    }

    private void navigateToActivity(Class activityClass){
        Tools.addLog("navigateToActivity "+activityClass.toString());
        //Intent de la classe passée en paramètre
        Intent intent = new Intent(this, activityClass);
        startActivity(intent);
    }
}
