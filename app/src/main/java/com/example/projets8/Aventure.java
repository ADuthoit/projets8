package com.example.projets8;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class Aventure extends AppCompatActivity {

    String TAG = "AventureMode";

    Button boutonRetourCamera;
    Button boutonRetourMenu;

    String description;
    TextView textView;
    ImageView imageView;
    TextView chapitre;

    int comptChapitre;

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getAssets().open("chapitres.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aventure);

        chapitre = findViewById(R.id.chapitre);

        if (getIntent().getExtras() != null) {
            comptChapitre = getIntent().getExtras().getInt("chapitre", 0);
            int numChap = comptChapitre + 1;
            chapitre.setText("Chapitre " + numChap);
            Log.d("Aventure", "comptChapitre = " + comptChapitre);
        } else {
            comptChapitre = 0;
        }

        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset());
            JSONArray my_Array = obj.getJSONArray("histories");


            JSONObject jo_inside = my_Array.getJSONObject(comptChapitre);
            Log.d("Details-->", jo_inside.getString("description"));

            description = jo_inside.getString("description");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        boutonRetourCamera = findViewById(R.id.bouton_retrourCamAventure);
        boutonRetourCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                goToRetourCamera();
            }
        });

        boutonRetourMenu = findViewById(R.id.retourMenu);
        boutonRetourMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToMenu();
            }
        });


        textView = findViewById(R.id.textview_description);
        textView.setText(description);
        imageView = findViewById(R.id.imageview_history);

        switch (comptChapitre) {
            case 0:
                imageView.setImageResource(R.drawable.chapitre1);
                break;
            case 1:
                imageView.setImageResource(R.drawable.chapitre2);
                break;
            case 2:
                imageView.setImageResource(R.drawable.chapitre3);
                break;
            case 3:
                imageView.setImageResource(R.drawable.chapitre4);
                break;
            case 4:
                imageView.setImageResource(R.drawable.chapitre5);
                break;
            case 5:
                imageView.setImageResource(R.drawable.chapitre6);
                break;
            case 6:
                imageView.setImageResource(R.drawable.chapitre7);
                break;
            case 7:
                imageView.setImageResource(R.drawable.chapitre8);
                break;
            case 8:
                imageView.setImageResource(R.drawable.chapitre9);
                break;
            case 9:
                imageView.setImageResource(R.drawable.chapitre10);
                break;
            case 10:
                imageView.setImageResource(R.drawable.chapitre11);
                break;
            case 11:
                imageView.setImageResource(R.drawable.chapitre12);
                break;
            case 12:
                imageView.setImageResource(R.drawable.chapitre13);
                break;
            case 13:
                imageView.setImageResource(R.drawable.chapitre14);
                break;
            case 14:
                imageView.setImageResource(R.drawable.chapitre15);
                break;
            case 15:
                imageView.setImageResource(R.drawable.chapitre16);
                break;
            case 16:
                imageView.setImageResource(R.drawable.chapitre17);
                break;
            case 17:
                imageView.setImageResource(R.drawable.chapitre18);
                break;
            case 18:
                imageView.setImageResource(R.drawable.chapitre19);
                break;
            default:
                System.out.print("Invalid compt");
                break;
        }

    }

    private void goToRetourCamera() {
        Intent intent = new Intent(this, RetourCameraAventure.class);
        intent.putExtra("mode", "aventure");
        intent.putExtra("chapitre", comptChapitre);
        startActivity(intent);
    }

    private void goToMenu() {
        Intent intent = new Intent(this, Modes.class);
        intent.putExtra("avancéJoueur", comptChapitre);
        startActivity(intent);
    }
}
