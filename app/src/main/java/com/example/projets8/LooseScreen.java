package com.example.projets8;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class LooseScreen extends AppCompatActivity {

    TextView textView;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loose_screen);

        textView =findViewById(R.id.score);
        button =findViewById(R.id.button);

        int score = getIntent().getExtras().getInt("score", -1);

        textView.setText("Votre score : " + score + " Bravo !");

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToMenu();
            }
        });

    }



    private void goToMenu(){
        Intent intent = new Intent(this, Modes.class);
        startActivity(intent);
    }
}
