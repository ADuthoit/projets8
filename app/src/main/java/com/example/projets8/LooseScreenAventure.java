package com.example.projets8;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class LooseScreenAventure extends AppCompatActivity {

    Button button;
    int currentChapitre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loose_screen_aventure);

        button = findViewById(R.id.button);
        currentChapitre = getIntent().getExtras().getInt("chapitre", 0);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToAventureLoose();
            }
        });

    }

    private void goToAventureLoose() {
        Intent intent = new Intent(this, Aventure.class);
        intent.putExtra("chapitre", currentChapitre);
        startActivity(intent);
    }
}
