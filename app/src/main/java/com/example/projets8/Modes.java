package com.example.projets8;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.projets8.tools.Tools;

public class Modes extends AppCompatActivity {

    Button aventure;
    Button versus;
    Button aleatoire;
    Button informations;
    Button aPropos;

    int avancéJoueur = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modes);

        if(getIntent().getExtras() != null){
            avancéJoueur = getIntent().getExtras().getInt("avancéJoueur",0);
            Log.d("Menu", "Le joueur est arrivé au chapitre  "+(avancéJoueur+1));
        }

        aventure = findViewById(R.id.aventure);
        versus = findViewById(R.id.versus);
        aleatoire = findViewById(R.id.alea);

        aventure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { goToAcceuilAventure();
            }
        });

        aleatoire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { navigateToActivity(Aleatoire.class);
            }
        });



    }


    private void navigateToActivity(Class activityClass){
        Tools.addLog("navigateToActivity "+activityClass.toString());
        //Intent de la classe passée en paramètre
        Intent intent = new Intent(this, activityClass);
        startActivity(intent);
    }

    private void goToAcceuilAventure(){
        Tools.addLog("navigateToActivity Aventure");
        Intent intent = new Intent(this, AcceuilAventure.class);
        intent.putExtra("avancéJoueur",avancéJoueur);
        startActivity(intent);
    }





}
