package com.example.projets8;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class AcceuilAventure extends AppCompatActivity {

    Button boutonJouer;
    Button boutonReprendre;
    Button boutonRecommencer;

    int avancéJoueur = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acceuil);

        if (getIntent().getExtras() != null) {
            avancéJoueur = getIntent().getExtras().getInt("avancéJoueur", 0);
            Log.d("Acceuil_Aventure", "On sait que le joueur s'est arrété au chapitre " + (avancéJoueur + 1));
        }

        boutonJouer = findViewById(R.id.bouton_jouer);
        boutonJouer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToAventure();
            }
        });

        boutonRecommencer = findViewById(R.id.bouton_recommencer);
        boutonRecommencer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToAventure();
            }
        });

        boutonReprendre = findViewById(R.id.bouton_reprendre);
        boutonReprendre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                returnToAventure();
            }
        });

        if (avancéJoueur == 0) {
            boutonReprendre.setVisibility(View.GONE);
            boutonRecommencer.setVisibility(View.GONE);

        } else {
            boutonJouer.setVisibility(View.GONE);
        }
    }

    private void goToAventure() {
        Intent intent = new Intent(this, Aventure.class);
        startActivity(intent);
    }

    private void returnToAventure() {
        Intent intent = new Intent(this, Aventure.class);
        intent.putExtra("chapitre", avancéJoueur);
        startActivity(intent);
    }

}